#!/bin/bash
docker stop $IMAGE_NAME && docker rm $IMAGE_NAME
docker container run -itd -p 8090:80 --name $IMAGE_NAME $IMAGE_NAME